#For this analysis we utilized 8 CPUs per task and 4G memory per CPU on the Duke Compute Cluster

#Use preprocessed databases to generate a genomes storage database
anvi-gen-genomes-storage \
-e map.txt \
-o genomes-GENOMES.db

#Construct pangenome
anvi-pan-genome \
-g genomes-GENOMES.db \
--project-name pangenome \
--output-dir 03_pan \
--num-threads 8 \
--minbit 0.5 \
--mcl-inflation 10 \
--use-ncbi-blast \
--min-occurrence 2

#Calculate average nucleotide identity between all genomes using the pyANI method
anvi-compute-genome-similarity \
--external-genomes map.txt \
--program pyANI \
--output-dir 04_ANI \
--num-threads 8 \
--pan-db 03_pan/pangenome-PAN.db

#Import phylogroup classification as metadata
anvi-import-misc-data \
-p 03_pan/pangenome-PAN.db \
-t layers \
layers.txt

#Create a default collection to summarize every gene cluster in the pangenome
anvi-script-add-default-collection \
-c genomes-GENOMES.db \
-p 03_pan/pangenome-PAN.db

anvi-summarize \
-p 03_pan/pangenome-PAN.db \
-g genomes-GENOMES.db \
-c DEFAULT \
-o PROJECT-SUMMARY

#Extract and concatenate single-copy core protein sequences
anvi-get-sequences-for-gene-clusters \
-g genomes-GENOMES.db \
-p 03_pan/pangenome-PAN.db \
-o core_protein_alignment.fa \
-C default \
--bin-id core \
--concatenate-gene-clusters

#Generate a phylogenetic tree of single-copy core protein sequences
anvi-gen-phylogenomic-tree \
-f core_protein_alignment.fa \
-o core_protein_tree.txt

# Estimate functional enrichment
anvi-compute-functional-enrichment-in-pan \
-p 03_pan/pangenome-PAN.db \
-g genomes-GENOMES.db \
--category clade \
--annotation-source COG20_FUNCTION \
-o clade_enriched_functions.txt \
--functional-occurrence-table-output clade_functions_occurrence_frequency.txt

#Extract 16S rRNA sequences for each genome
for f in 02_databases/*.db
do
SAMPLE=${f%%.*}
SAMPLE=${SAMPLE#*/}
anvi-get-sequences-for-hmm-hits -c 02_databases/$SAMPLE.db \
--hmm-source Ribosomal_RNA_16S \
-o 06_16S/$SAMPLE.fasta
done
