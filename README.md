# The role of *Akkermansia* species and Phylogroup on Human Health

This is indended to supplement the methods described in Mueller et al 2024, "The role of *Akkermansia* species and Phylogroup on Human Health".

## Usage of anvi'o
The comparison of 234 *Akkermansia* genomes was performed using the pangenomic workflow in anvi'o 7.1.

The commands used to process fasta files into a pangenome can be found in [anvi'o Preprocessing](https://gitlab.oit.duke.edu/kdm65/akkermansia-species-and-phylogroups/-/blob/main/anvi'o%20Preproccessing?ref_type=heads). 
* The fasta files were converted into contigs-fasta format to meet the needs of further anvi'o programs using the parameters --simplify_names and --seq-type NT.
* The reformatted files were then used to generate contigs databases using anvi-gen-contigs-database. The resulting databases were annotated using anvi-run-hmms, anvi-run-cogs, and anvi-run-kegg-kofams.
* These databases were then used to generate a genomes storage database using anvi-gen-genomes storage.
* The pangenome was then constructed using anvi-pan-genome with the parameters --minbit 0.5, --mcl-inflation 10, and --use-ncbi-blast. 

The commands used to perform comparitive analysis of the *Akkermansia* pangenome within anvi'o can be found in [anvi'o Pangenome](https://gitlab.oit.duke.edu/kdm65/akkermansia-species-and-phylogroups/-/blob/main/anvi'o%20Pangenome?ref_type=heads).
* Average nucleotide identity (ANI) between all genomes was calculated using anvi-compute-genome-similarity using the pyANI method. Based on the results of pyANI, each genome was assigned to a phylogroup. Phylogroup classification was then assigned as metadata using anvi-import-misc-data.
* A default collection was created to summarize every gene cluster in the pangenome using anvi-script-add-default-collection and anvi-summarize. The core gene clusters unique to each species and those shared between species were determined by filtering for those gene clusters present in all genomes of at least one species. Additionally, the protein sequences of the single-copy core gene clusters shared with *Akkermansia glycaniphila* were extracted and concatenated using anvi-get-sequences-for-gene-clusters, and anvi-gen-phylogenomic-tree was used to generate a phylogenetic tree of those sequences.
* Estimation of functional enrichment was performed using anvi-estimate metabolism using the parameters --module-completion-threshold 0.5.
* Representative full-length 16S rRNA sequences for each genome were extracted using anvi-get-sequences-for-hmm-hits using the parameter --hmm-source Ribosomal_RNA_16S. Alignment of these sequences and tree generation was then performed using Clustal Omega.

## Analysis of metagenomic sequencing data

### biobakery
The commands used to process metagenomic reads using three biobakery tools can be found in [biobakery Processing](https://gitlab.oit.duke.edu/kdm65/akkermansia-species-and-phylogroups/-/blob/main/biobakery%20Processing).
Reference genomes used:
* *A. muciniphila* AmIa strain was Muc<sup>T</sup>
* *A. muciniphila* AmIb strain was Akk1570
* *A. massiliensis* strain was Akk0580
* *A. biwaensis* strain was Akk0196

**KneadData**
KneadData was applied to raw metagenomic sequencing reads to perform quality control.

**MetaPhlAn 3.0**
MetaPhlAn 3.0 was then used to determine the relative abundance of genus *Akkermansia* in all samples.

**StrainPhlAn 3.0**
StrainPhlAn 3.0 was then used to determine the dominant species or phylogroup present in all samples. 
* To identify species and phylogroup, we also run POMMS samples for which we also had previously identified and isolated *Akkermansia*. These samples of known species and phylogroup thus act as controls. 
* The resulting concatenated alignment files were then subjected to Kimura 2-parameter correction using the distmat function of EMBOSS.
* The species and phylogroup of *Akkermansia* in any given sample wass then determined based on which control has the shortest distance to that sample.

### SMEG
The commands used to set up a SMEG database and run SMEG on samples can be found in [SMEG Processing](https://gitlab.oit.duke.edu/kdm65/akkermansia-species-and-phylogroups/-/blob/main/SMEG%20Processing)

We generated a SMEG database using the below 34 *Akkermansia* genomes representing difference species and phylogroups. SMEG clusters are defined during database generation based on SNP prevalence among reference genomes, and the predicted *Akkermansia* species or phylogroup is determined based on what cluster our control POMMS samples are assigned to during growth rate estimation. SMEG estimates the growth rate (module growth_est) for bacteria in each sample by calculating the coverage of SNPs closer to the origin of replication, as compared to those closer to the terminus region. Therefore, the presence of *Akkermansia* in a sample is indicated by a non-zero estimated growth rate of those clusters.

    For example, we have control POMMS samples:
        * POMMS1 we have isolated *A. muciniphila* AmIa
        * POMMS2 we have isolated *A. muciniphila* AmIb
        * POMMS3 we have isolated *A. massiliensis*
        * POMMS4 we have isolated *A. biwaensis*
    We also have several samples with unknown *Akkermansia* species/phylogroup, Sample1, Sample2, Sample3, and Sample4.
    After running SMEG, we find that:
        * POMMS1 has a non-zero growth rate for cluster 2. This means that cluster 2 corresponds to *A. muciniphila* AmIa.
        * POMMS2 has a non-zero growth rate for cluster 3. This means that cluster 3 corresponds to *A. muciniphila* AmIb.
        * POMMS3 has a non-zero growth rate for cluster 1. This means that cluster 1 corresponds to *A. massiliensis*.
        * POMMS4 has a non-zero growth rate for cluster 4. This means that cluster 4 corresponds to *A. biwaensis*.
    Our SMEG results also show us that:
        * Sample1 has a non-zero growth rate for cluster 4. This means that Sample1 contains *A. biwaensis*.
        * Sample2 has a non-zero growth rate for cluster 1. This means that Sample2 contains *A. massiliensis*.
        * Sample3 has a non-zero growth rate for clusters 2 and 3. This means that Sample3 contains *A. muciniphila* AmIa and AmIb.
        * Sample4 has a non-zero growth rate for cluster 3. This means that Sample4 contains *A. muciniphila* AmIb.

Reference genomes used:
* *A. muciniphila* AmIa
    * Akk0096
    * Akk0200
    * Akk0500a
    * Akk1713
    * Akk2670
    * Muc<sup>T</sup>
* *A. muciniphila* AmIb
    * Akk0500b
    * Akk0880
    * Akk1370
    * Akk1376
    * Akk14745
    * Akk1570
    * Akk1576
    * Akk1610
    * Akk1613
    * Akk1616
    * Akk1756
    * Akk1990
    * Akk2030
    * Akk2090
* *A. massiliensis*
    * Akk0580
    * Akk1476
    * Akk1573
    * Akk1863
    * Akk2000
    * Akk2190
    * Akk2196
    * Akk2680
    * AkkB40
* *A. biwaensis*
    * Akk0196
    * Akk0490
    * Akk0496a
    * Akk0496b
    * Akk2750

### StrainR
The commands used to set up a StrainR database and run StrainR on samples can be found in [StrainR Processing](https://gitlab.oit.duke.edu/kdm65/akkermansia-species-and-phylogroups/-/blob/main/StrainR%20Processing).

StrainR runs on one sample at a time, and the output is likewise for one sample. We have written a Python script, [summarize_StrainR](https://gitlab.oit.duke.edu/kdm65/akkermansia-species-and-phylogroups/-/blob/main/summarize_StrainR.py?ref_type=heads), which takes a folder containing all StrainR results and compiles those results into one dataframe.

We generated a StrainR database using one representative *Akkermansia* genome per species and phylogroup. We then performed sample mapping, which resulted in the assignment of a StrainR value to each species and phylogroup in all samples. We next determined the proportion of each phylogroup in a sample, based on the summed StrainR values for that sample. These proportions are then multipled by the relative abundance of *Akkermansia*, as determined by MetaPhlAn 3.0, to determine the relative abundance of each phylogroup.

    For example, we have several samples with unknown *Akkermansia* species/phylogroup for which we have run MetaPhlAn 3.0.
        * Sample1 has 5% *Akkermansia* relative abundance
        * Sample2 has 1% *Akkermansia* relative abundance
        * Sample3 has 2% *Akkermansia* relative abundance
        * Sample1 has 10% *Akkermansia* relative abundance
    After running StrainR, we find:
        * The StrainR values of Sample1 are 250 (A.muc AmIa), 0 (AmIb), 0, (A.mass), 0 (A.biw)
        * The StrainR values of Sample1 are 0 (A.muc AmIa), 300 (AmIb), 0, (A.mass), 0 (A.biw)
        * The StrainR values of Sample1 are 0 (A.muc AmIa), 0 (AmIb), 0, (A.mass), 180 (A.biw)
        * The StrainR values of Sample1 are 100 (A.muc AmIa), 300 (AmIb), 0, (A.mass), 0 (A.biw)
    Thus:
        * Sample1 has an *A. muciniphila* AmIa relative abundance of 5%, and 0% relative abundance of the other species/phylogroup.
        * Sample2 has an *A. muciniphila* AmIb relative abundance of 1%, and 0% relative abundance of the other species/phylogroup.
        * Sample3 has an *A. biwaensis* relative abundance of 2%, and 0% relative abundance of the other species.
        * Sample4 has an *A. muciniphila* AmIa relative abundance of 2.5% (=(100/(100+300))*10%), an *A. muciniphila* AmIb relative abundance of 7.5%, and 0% relative abundance of the other species.

Reference genomes used:
* *A. muciniphila* AmIa strain was Muc<sup>T</sup>
* *A. muciniphila* AmIb strain was Akk1570
* *A. massiliensis* strain was Akk0580
* *A. biwaensis* strain was Akk0196

## Analysis of 16S rRNA sequencing data
The R script used to identify *Akkermansia* species from 16S rRNA community sequencing can be found in [16S.rmd](https://gitlab.oit.duke.edu/kdm65/akkermansia-species-and-phylogroups/-/blob/main/16S.rmd?ref_type=heads). 
* To run this analyis, you will need to assemble a fasta file containing the 16S rRNA sequences of *Akkermansia* isolates for which you know the species. For the Mueller et al 2024 analyis, we utilized the V3-V4 subregion of isolates obtained in Becken et al 2021 to match the sequencing performed on the POMMS samples. Alignment of these known sequences, when visualized as a tree, should result in clustering by species
* In the example below, we have aligned three ASVs (POMMS1, POMMS2, and POMMS3) with several sequences from known isolates. In this example, POMMS1 clusters with the *A. muciniphila* isolates (marked in green), POMMS2 clusters with the *A. biwaensis* (marked in blue) isolates, and POMMS3 clusters with the *A. massiliensis* isolates (marked in purple). Thus, when editing our taxa table as described in 16S.rmd, we will change the species identifier of the POMMS1 ASV to *A. muciniphila*, POMMS2 ASV to *A. biwaensis*, and POMMS3 to *A. massiliensis*. Continue this process for as many genus *Akkermansia* ASVs as are in your dataset.
![alt text](https://gitlab.oit.duke.edu/kdm65/akkermansia-species-and-phylogroups/-/raw/main/ASV_identification.png?ref_type=heads "Example"){width=750, height=750}

## Run environments
Pangenomic comparisons and analysis of metagenomic sequencing data was performed on the Duke Compute Cluster.

Analysis of 16S rRNA sequencing data was performed using a personal computer
* Intel(R) Core(TM) i7-9750H CPU @ 2.60 GHz processor
* 16 GB RAM
* Windows 10 Home version 22H2

## Authors and acknowledgments
Katherine D. Mueller, M. Emilia Panzetta, Lauren Davey, Jessica R. McCann, John F. Rawls, Gilberto E. Flores, Raphael H. Valdivia

Citations may be found in Mueller et al 2024.
Links to each program:
* [anvi'o](https://anvio.org/)
* [biobakery](https://huttenhower.sph.harvard.edu/tools/)
* [StrainR](https://github.com/turnbaughlab/StrainR)
* [SMEG](https://github.com/ohlab/SMEG)

